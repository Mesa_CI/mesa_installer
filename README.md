# Mesa Installer
to test container run following from "mesa_build" root directory:
```bash
docker run -it \
	   -v ./scripts:/src \
	   -v ./mesa_build:/opt/mesa \
	   mesa_builder_tar:latest \
	   sh
```

### To build manually inside the container run following command:
```you can always append -h/--hellp to get more info```

```bash
docker-compose run tar python3 -u build_mesa.py
```


### Troubleshooting
if buld timing out on ubuntu servers 
```bash
sudo apt clean && sudo apt autoclean
```