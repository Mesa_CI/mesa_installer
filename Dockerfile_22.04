# Set none accessible variables 
ARG DEBIAN_FRONTEND=noninteractive \
    DEFAULT_CODE_VERSION=22.04 

FROM ubuntu:${DEFAULT_CODE_VERSION}

ENV PYTHONDONTWRITEBYTECODE 1 \
    PYTHONUNBUFFERED 1 

# define work directory
WORKDIR /src

# Build dependency for MESA
RUN apt-get update && apt-get install -y --show-progress \
    bison \
    build-essential \
    ccache \
    cmake \
    flex \
    g++-multilib \
    gcc-multilib \
    git \
    glslang-tools \
    libc6 \
    libc6-dev \
    libcaca0 \
    libclang-15-dev \
    libclang-cpp15-dev \
    libclc-15 \
    libclc-15-dev \
#    libclc-dev \
    libdrm-dev \
    libdrm2 \
    libedit-dev \
    libegl-mesa0 \
    libegl1-mesa-dev \
    libelf-dev \
    libepoxy-dev \
    libexpat1-dev \
    libgbm-dev \
    libgbm1 \
    libgl1-mesa-dev \
    libgl1-mesa-dri \
    libglapi-mesa \
    libglu1-mesa \
    libglu1-mesa-dev \
    libglx-mesa0 \
    libgtk-3-dev \
    libicu-dev \
    libjpeg-dev \
    libllvmspirvlib-15-dev \ 
    libllvmspirvlib15 \
    libncurses-dev \
    libomxil-bellagio-dev \
    libosmesa6 \
    libosmesa6-dev \
    libpciaccess-dev \
    libpciaccess0 \
    libpng-dev \
    librust-bindgen-dev \
    libsensors5 \
    libssl-dev \
    libtinfo-dev \
    libtool \
    libudev-dev \
    libunwind-15 \
    libva-dev \
    libvdpau-dev \
    libvulkan-dev \
    libvulkan1 \
    libwayland-cursor0 \
    libwayland-egl-backend-dev \
    libwayland-egl1-mesa \
    libx11-dev \
    libx11-xcb-dev \
    libxatracker-dev \
    libxatracker2 \
    libxcb-dri2-0-dev \
    libxcb-dri3-dev \
    libxcb-glx0-dev \
    libxcb-present-dev \
    libxcb-randr0-dev \
    libxcb-shm0 \
    libxcb-shm0-dev \
    libxcb-sync-dev \
    libxcb-xfixes0-dev \
    libxdamage-dev \
    libxext-dev \
    libxfixes-dev \
    libxkbcommon0 \
    libxml2-dev \
    libxrandr-dev \
    libxrandr2 \
    libxrender1 \
    libxshmfence-dev \
    libxxf86vm-dev \
    libzstd-dev \
    libzstd1 \
    linux-libc-dev \
    llvm-15 \
    mesa-utils \
    ninja-build \
    pkg-config \
    python3-distro \
    python3-git \
    python3-mako \
    python3-pip \
    python3-requests \
    python3-rich \
    tar \
    valgrind && \
#    meson \ # meson 1.1.0 not available under 22.04
#    python3-ply \ doesn't work
    rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install meson \
    ply

# LLVM-Config version fix
RUN rm /usr/bin/llvm-config && \
    ln -s /usr/bin/llvm-config-15 /usr/bin/llvm-config

VOLUME /opt/mesa
VOLUME /src
