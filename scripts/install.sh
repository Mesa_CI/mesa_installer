#!/bin/bash
# Copyright (C) Intel Corp.  2024.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>
#  **********************************************************************/


install_mesa=true # Default setting
LDCONF="/etc/ld.so.conf.d/01-mesa.conf"
VK_ICD_CONF="/etc/profile.d/01-mesa-current.sh"


check_sudo(){
   if [ ! "$EUID" -eq 0 ]; then
      echo "The installer needs to be executed with sudo."
      exit 1
   fi
}

set_ldconfig() {
   echo "Installing ldconfig settings to $LDCONF..."
   rm -rf $LDCONF

   cat > $LDCONF << EOF
$TAR_BASE_DIR/$TAR_INSTALL_DIR/64/lib
$TAR_BASE_DIR/$TAR_INSTALL_DIR/32/lib
$TAR_BASE_DIR/$TAR_INSTALL_DIR/64/lib/x86_64-linux-gnu
$TAR_BASE_DIR/$TAR_INSTALL_DIR/32/lib/i386-linux-gnu
EOF

   ldconfig
}

set_vk_icd(){
   echo "Setting up default VK_ICD_FILENAMES..."
   local VK_ICD_FILENAMES="$TAR_BASE_DIR/$TAR_INSTALL_DIR/64/share/vulkan/icd.d/intel_icd.x86_64.json:$TAR_BASE_DIR/$TAR_INSTALL_DIR/32/share/vulkan/icd.d/intel_icd.i686.json"

   rm $VK_ICD_CONF

   cat > $VK_ICD_CONF << EOF
VK_ICD_FILENAMES=${VK_ICD_FILENAMES}
EOF
}

untar_payload (){
   # Get where where payload starts
   TAR_PAYLOAD=$(($(grep --text --line-number '^__PAYLOAD_BELOW__:$' $0 | cut -d ':' -f 1) + 1))

   # directory where a binary executable is to be saved
   TMPDIR=$(mktemp -d /tmp/mesa_installer.XXXXXXX)

   # extract the embedded binary tarbal
   tail -n +${TAR_PAYLOAD} $0 | tar -C "${TMPDIR}" -xzf -
   echo "Done Extracting: $TMPDIR"
}

install(){
   echo "Extracting tarball"
   untar_payload

   # Compare build with running OS
   if [ -e /etc/lsb-release ]; then
      eval $(cat /etc/lsb-release)
      # declare -l $DISTRIB_ID
         if [ "${DISTRIB_ID,,}" != "${TAR_DISTRIB_ID,,}" ] && [ "{$DISTRIB_RELEASE,,}" != "${TAR_DISTRIB_RELEASE,,}" ]; then
            echo "ERROR: Installer requires $TAR_DISTRIB_ID:$TAR_DISTRIB_RELEASE"
            exit 1
         fi
   else
      DISTRIB_ID=$(lsb_release -si)
      DISTRIB_RELEASE=$(lsb_release -sr)
      if [ "${DISTRIB_ID,,}" != "${TAR_DISTRIB_ID,,}" ] && [ "{$DISTRIB_RELEASE,,}" != "${TAR_DISTRIB_RELEASE,,}" ]; then
         echo "ERROR: Installer requires $TAR_DISTRIB_ID:$TAR_DISTRIB_RELEASE"
         exit 1
      fi
   fi

   # mesa build
   echo "Setting MESA Directory"

   echo "Checking old mesa install path $TAR_BASE_DIR/$TAR_INSTALL_DIR"
   if [ -d "$TAR_BASE_DIR" ]; then
      echo "Found old directory. Removing it"
      if [ -d "$TAR_BASE_DIR/$TAR_INSTALL_DIR" ]; then
         rm -rf "${TAR_BASE_DIR:?}"
         mkdir -p "$TAR_BASE_DIR/$TAR_INSTALL_DIR/64"
      else
         mkdir -p "$TAR_BASE_DIR/$TAR_INSTALL_DIR/64"
      fi
   else
      mkdir -p "$TAR_BASE_DIR"/"$TAR_INSTALL_DIR"/64
   fi
   
   # mkdir $TAR_BASE_DIR/$TAR_INSTALL_DIR x64
   ls -la "$TMPDIR/$TAR_INSTALL_DIR"
   mv -v "$TMPDIR/$TAR_INSTALL_DIR/include" "$TAR_BASE_DIR/$TAR_INSTALL_DIR/64/"
   mv -v "$TMPDIR/$TAR_INSTALL_DIR/lib" "$TAR_BASE_DIR/$TAR_INSTALL_DIR/64/"
   mv -v "$TMPDIR/$TAR_INSTALL_DIR/share" "$TAR_BASE_DIR/$TAR_INSTALL_DIR/64/"
   #ToDo add 32bit later

   # set_link
   set_ldconfig
   set_vk_icd
}

uninstall(){
   echo "Removing: $LDCONF"
   rm -rf $LDCONF
   echo "Removing: $VK_ICD_CONF"
   rm -rf $VK_ICD_CONF
   echo "Removing"
   rm -rf "${TAR_BASE_DIR:?}"
   ldconfig
}

help(){
   clear
   echo "Avalable Arguments to pass"
   echo "-h  --help              To see this help again!"
   echo
   echo "-i  -install            Will build tar and install in default location."
   echo "-u  -uninstall          Will remove associated MESA files from defult location."
   echo
}

for i in "$@"; do
   case $i in
   -i|--install)
      install_mesa=true
      shift
      ;;
   -u|--uninstall)
      install_mesa=false
      shift
      ;;
   -h|--help)
      help
      shift
      ;;
   -*|--*)
      echo "Unknown option $i"
      help
      exit 1
      ;;
   *)
      ;;
  esac
done

if [ "$install_mesa" == true ]; then
   echo "Installing MESA"
   check_sudo
   install
else
   echo "Removing MESA"
   check_sudo
   uninstall
fi

exit 0
### DO NOT EDIT BELOW ###
