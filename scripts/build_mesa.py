#!/usr/bin/env python3
"""
Copyright (C) Intel Corp.  2024.  All Rights Reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice (including the
next paragraph) shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

***********************************************************************
* Authors:                                                            *
*   Yuriy Bogdanov <yuriy.bogdanov@intel.com>                         *
***********************************************************************
"""


import argparse
import os
import subprocess
from urllib.parse import urlparse
import progress
import distro
import requests
import git
# NOTE: default build directories are provided by argparser
# NOTE: default path: /opt/mesa
# NOTE: build repo:   /opt/mesa/repo
# NOTE: build path:   /opt/mesa/build
# NOTE: install path  /opt/mesa/install

PROXY = "http://proxy-jf.intel.com:912"
DEFAULT_MESA_DIR = '/opt/mesa'
LATEST_SCRIPT_BUILD_DIR = '/current'

# Flag --repo will append to this list
DEFAULT_MORRORS = [
    'https://gitlab.freedesktop.org/mesa/mesa.git', # default mirror
    'git://otc-mesa-ci.jf.intel.com/git/mirror/mesa/origin'
]

# mason intel setup flaggs
# cmd: meson setup <build path> <--prefix MESA_INSTALL_PATH> <-flags>
intel_flags = ["-Dbuildtype=release",
                "-Dvulkan-drivers=intel",
                "-Dplatforms=x11",
                "-Dgallium-drivers=swrast,iris",
                "-Dllvm=enabled"]


def check_sha(repo, getsha):
    """ Check if sha exist """
    try:
        print(f"MESA SHA: {repo.rev_parse(getsha)}")
    except (ValueError, git.BadName)  as err: # sha provided is not in orign/main
        print(f"Could not find provided SHA, Error: {err}")


def proxy_check(mirror):
    """ Try setting proxy if intel network restricts access to gitlab """

    # from urlparse import urlparse  # Python 2
    parsed_uri = urlparse( mirror )
    host_uri = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)

    try:
        response = requests.get(host_uri, timeout=15).status_code
    except:
        response = 1

    if response != 200:
        print("Proxy detected: Setting proxy")
        os.environ['HTTP_PROXY'] = PROXY
        os.environ['HTTPS_PROXY'] = PROXY


# TODO change verbose mode from print to logging
def run_cmd(command, cwd=None, shell=False,):
    """ Execute system commands """
    print( ' '.join(str(i) for i in command) )
    if cwd:
        process = subprocess.Popen(command, cwd=cwd,
                                shell=shell,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                universal_newlines=True)
    else:
        process = subprocess.Popen(command,
                               shell=shell,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               universal_newlines=True)
    # This is needed for docker conteiner log output, can be change from print to logging later
    for line in process.stdout:
        print(line, end='')
    process.wait()


def checkout (_current_sha, base_path, src_path):
    """ Checkout git """
    repo_dir = os.path.join(base_path, src_path)
    latest_sha = _current_sha
    # try clonining first
    print(f"Default git: {DEFAULT_MORRORS[0]}")
    try:
        repo = git.Repo.clone_from(
            url=DEFAULT_MORRORS[0],
            to_path=repo_dir,
            progress=progress.GitRemoteProgress(),
        )
        latest_sha = repo.commit(_current_sha)
        print(f"Found commit 1: {latest_sha}\n"
              f"Published by: {latest_sha.author.name}\n"
              f"Date:\t      {latest_sha.authored_datetime}\n")
    except git.GitCommandError:
        # if repo exist pull latest
        repo = git.Repo(repo_dir)
        repo.remote('origin').pull('main', progress=progress.GitRemoteProgress())
        latest_sha = repo.commit(_current_sha)
        print(f"Found commit 2: {latest_sha}\n"
              f"Published by: {latest_sha.author.name}\n"
              f"Date:\t      {latest_sha.authored_datetime}\n")
    except Exception as err:
        print(f"Failed to get repository: {err}")

    # Set HEX value to latest_sha from default value
    if latest_sha == 'origin/main':
        for url in DEFAULT_MORRORS:
            try:
                if url != DEFAULT_MORRORS[0]:
                    remote = repo.create_remote('alt', url)
                    remote.fetch(_current_sha)
                repo.git.checkout(_current_sha)
                new_sha = repo.commit(_current_sha)
                print(f"Found commit 3: {new_sha}\n"
                      f"Published by: {new_sha.author.name}\n"
                      f"Date:\t      {new_sha.authored_datetime}\n")
            except git.GitCommandError as err:
                # Handle the case when the commit is not found in the repository
                if 'bad object' in str(err):
                    print(f"Commit {new_sha} not found in repository at: {url}")
                elif 'remote already exists' in str(err):
                    print(f"Repository:   {url}")
                    # clean up incase 'remote' exist in remote list
                    repo.delete_remote('alt')
                    remote = repo.create_remote('alt', url)
                    remote.fetch()

                    repo.git.checkout(_current_sha)
                    print(f"Found commit 4: {_current_sha}\n"
                          f"Published by: {_current_sha.author.name}\n"
                          f"Date:\t      {_current_sha.authored_datetime}\n")
                else:
                    # Handle other GitCommandError exceptions
                    print(f"Error in repository at {url}: {err}")
                    return -1
            except Exception as err:
                # Handle all other exceptions
                print(f"Error at repository at {url}: {err}")
                return -1
            finally:
                repo.delete_remote('alt')
    # remove short tag if you want full sha or specify number
    return repo.git.rev_parse(_current_sha, short=True)

def setup_mesa(base_path, src_path, build_path, install_path ):
    """ Setup meson and mesa """
    _cmd = ["meson", "setup", f"{base_path}/{build_path}",
                     "--prefix", f"{base_path}/{install_path}"]
    _cmd.extend(intel_flags)
    _cmd.append(f"{base_path}/{build_path}, {base_path}/{src_path}")
    if os.path.exists(f"{base_path}/{build_path}"):
        _cmd.append("--reconfigure")

    run_cmd(_cmd)

    # Run build and install
    _cmd = ["ninja", "-C", f"{base_path}/{build_path}", "install"]
    run_cmd(_cmd)


def build_tar(sha, base_path, mesa_src, install_path):
    """ Run tar against mesa install package """
    with open(f"{base_path}/{mesa_src}/VERSION", 'r', encoding='UTF-8') as ver_file:
        mesa_ver = ver_file.readline().strip('\n')

    tar_file = f"mesa_pkg_{sha}.tgz"

    if os.path.isfile(f"{base_path}/{tar_file}"):
        print(f"Removing old tar {tar_file}")
        os.remove(f"{base_path}/{tar_file}")

    _cmd = ["tar", "-C", base_path, "-cvzf", tar_file, install_path]
    run_cmd(_cmd, base_path) # base_path

    for old_tar in os.listdir(LATEST_SCRIPT_BUILD_DIR):
        print(f"Removing {old_tar}")
        os.remove(f"{LATEST_SCRIPT_BUILD_DIR}/{old_tar}")

    with open(f"/current/install_mesa_{mesa_ver}_{sha}.sh", 'wb+') as file:
        with open('install.sh', 'rb') as first:
            contents = first.readlines()

        contents.insert(33, f"TAR_DISTRIB_ID={distro.id()}\n".encode())
        contents.insert(33, f"TAR_DISTRIB_RELEASE={distro.version()}\n".encode())
        contents.insert(33, f"TAR_BASE_DIR={base_path}\n".encode())
        contents.insert(33, f"TAR_INSTALL_DIR={install_path}\n".encode())
        file.seek(0)
        file.truncate()

        for line in contents:
            file.write(line)

        file.write('__PAYLOAD_BELOW__:\n'.encode())
        with open(f"{base_path}/{tar_file}", 'rb') as last:
            file.write(last.read())

    os.chown(f"/current/install_mesa_{mesa_ver}_{sha}.sh", 1000, 1000)
    os.chmod(f"/current/install_mesa_{mesa_ver}_{sha}.sh", 0o775)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description=
                                        "Build and tar all files MESA Driver",
                                        formatter_class=argparse.RawTextHelpFormatter)
    argparser.add_argument("--mesa_base", default=DEFAULT_MESA_DIR,
                           help="Default mesa directory. (Default: /opt/mesa)")
    argparser.add_argument("--mesa_src", default='src',
                           help="Directory where MESA sourse points. (Default: /opt/mesa/src)")
    argparser.add_argument("--mesa_build", default='build',
                           help="Directory where MESA bulds points. (defult: /opt/mesa/build)")
    argparser.add_argument("--mesa_install", default='install',
                           help="Directory where MESA install points. (defult: /opt/mesa/install)")
    argparser.add_argument("--repo", help="Add custom/your git repository for mesa cache")
    argparser.add_argument("--sha", default='origin/main',
                           help="Provide MESA SHA you want to build driver base on "\
                            "(Default: latest mesa sha)")
    args = argparser.parse_args()

    if args.repo:
        DEFAULT_MORRORS.append(args.repo)

    # create /opt/mesa if it doesnot exist or passed custom base path
    if args.mesa_base != DEFAULT_MESA_DIR:
        print("Creating mesa repository directory")
        os.mkdir(args.mesa_base, mode=0o776)
        DEFAULT_MESA_DIR = args.mesa_base

    # Check Proxy
    proxy_check(DEFAULT_MORRORS[0])

    # Checkout mesa repository and return sha
    current_sha = checkout(args.sha, args.mesa_base, args.mesa_src)

    if current_sha != -1:
        setup_mesa(args.mesa_base, args.mesa_src, args.mesa_build, args.mesa_install)
        build_tar(current_sha, args.mesa_base, args.mesa_src, args.mesa_install)
    else:
        print(f"Failed to check out mesa sha, returned '{current_sha}' SHA is not valid.")
